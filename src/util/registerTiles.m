/*
 * registerTiles.m
 * Implements a function that registers terrain tiles
 * Created on 4/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
#import <Foundation/Foundation.h>
#import "registerTiles.h"
#import "../world/TileRegistry.h"
#import "../world/Tile.h"
#import "../world/TileAttribute.h"
#import "../world/TileID.h"

//private function declaration
static void registerTile(NSString* name, TileID tid, TileAttribute attrs,
				SDLRenderer* rend);

//registerTiles function - registers tiles
void registerTiles(SDLRenderer* rend) {
	//log that the process started
	NSLog(@"Registering tiles...");
	
	//register the tiles
	registerTile(@"grass", TILE_GRASS, WALK, rend);
	registerTile(@"tree", TILE_TREE, NOWALK, rend);
	registerTile(@"water", TILE_WATER, WALK | DMG_ENRGY, rend);
	registerTile(@"sand", TILE_SAND, WALK, rend);
	
	//log that the process was successful
	NSLog(@"Tiles registered!");
}

//private registerTile function - registers an individual tile
static void registerTile(NSString* name, TileID tid, TileAttribute attrs,
				SDLRenderer* rend) {
	//register the tile
	[[TileRegistry sharedRegistry] registerTile:
		[[Tile alloc] initWithBounds:
		  [[SDLRect alloc] initWithX: 0 andY: 0
			  		andWidth: 32 andHeight: 32]
			andTexture: [[SDLTexture alloc] initWithImage:
					[[SDLImgElement alloc] 
						initWithImagePath:
						[[NSBundle mainBundle] 
						pathForResource: name 
							 ofType: @"png"
						    inDirectory: @"tiles"]]
					andRenderer: rend]
		  andID: tid 
		  andAttrs: attrs]];	
}

//end of file
