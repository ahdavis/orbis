/*
 * TileRegistry.m
 * Implements a class that manages terrain tiles
 * Created on 4/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import the class header
#import "TileRegistry.h"

//private method declarations
@interface TileRegistry ()

//initializes a TileRegistry
- (id) init;

@end //end of private methods

//class implementation
@implementation TileRegistry

//declare a static field
static TileRegistry* instance = nil;

//property synthesis
@synthesize ids = _ids;

//sharedRegistry class method - returns an instance of the registry
+ (TileRegistry*) sharedRegistry { 
	//create the instance if one is not already created
	if(instance == nil) {
		//initialize the instance
		instance = [[[self alloc] init] autorelease];
	}

	//and return the instance
	return instance;
}

//dealloc method - deallocates the registry
- (void) dealloc {
	[_registry release]; //release the registry 
	[_ids release]; //release the ids array
	[super dealloc]; //and call the superclass dealloc method
}

//hasTileWithID method - returns whether a Tile
//with a given ID is registered
- (bool) hasTileWithID: (TileID) tid {
	//loop and check for the tile
	for(Tile* tile in _registry) {
		if(tile.tileID == tid) {
			return YES;
		}
	}

	//if control reaches here, then the Tile was not found
	//so return false
	return NO;
}

//registerTile method - registers a Tile
- (bool) registerTile: (Tile*) tile {
	//make sure the tile has not already
	//been registered
	for(Tile* t in _registry) {
		if(t.tileID == tile.tileID) {
			//log that the registration failed
			NSLog(@"ID %i was already registered!", t.tileID);

			//and return with an error
			return NO;
		}
	}

	//register the tile
	[_registry addObject: tile];

	//add its ID to the ID array
	[_ids addObject: [[NSNumber alloc] initWithInt: tile.tileID]];

	//log that the registration succeeded
	NSLog(@"Registered a tile with ID %i!", tile.tileID);

	//and return a success
	return YES;
}

//tileForID method - returns the tile associated with
//a given tile ID
- (Tile*) tileForID: (TileID) tileID {
	//loop and return the tile
	for(Tile* t in _registry) {
		if(t.tileID == tileID) {
			return [t copy];
		}
	}

	//if control reaches here, then the ID was not registered
	//so we return nil
	return nil;
}

//tilesWithAttribute method - returns an array of
//all Tiles with a specified attribute
- (NSArray*) tilesWithAttribute: (TileAttribute) attr {
	//declare an accumulator array
	NSMutableArray* accum = [[NSMutableArray alloc] init];

	//loop and add tiles
	for(Tile* t in _registry) {
		if(t.attrs == attr) {
			[accum addObject: [t copy]];
		}
	}

	//create a non-mutable array from the accumulator
	NSArray* ret = [[accum copy] autorelease];

	//free the accumulator
	[accum release];

	//and return the array of specified tiles
	return ret;
}

//private init method - initializes a TileRegistry instance
- (id) init {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_registry = [[NSMutableArray alloc] init];
		_ids = [[NSMutableArray alloc] init];
	}

	//and return the instance
	return self;
}

@end //end of implementation
