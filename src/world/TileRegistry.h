/*
 * TileRegistry.h
 * Declares a class that manages terrain tiles
 * Created on 4/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
#import <Foundation/Foundation.h>
#import "Tile.h"
#import "TileID.h"
#import "TileAttribute.h"

//class declaration
@interface TileRegistry : NSObject {
	//fields
	NSMutableArray* _registry; //the tile registry
	NSMutableArray* _ids; //contains the tile IDs that are registered
}

//property declaration
@property (readonly) NSMutableArray* ids;

//method declarations

//returns an instance of the registry
+ (TileRegistry*) sharedRegistry;

//deallocates the registry
- (void) dealloc;

//returns whether a Tile with a given ID is
//currently registered
- (bool) hasTileWithID: (TileID) tid;

//adds a Tile to the registry and
//returns whether the Tile was registered successfully
- (bool) registerTile: (Tile*) tile;

//returns the Tile associated with a given ID
- (Tile*) tileForID: (TileID) tileID;

//returns all tiles with a given attribute
- (NSArray*) tilesWithAttribute: (TileAttribute) attribute;

@end //end of header
