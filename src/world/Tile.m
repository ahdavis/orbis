/*
 * Tile.m
 * Implements a class that represents a terrain tile
 * Created on 3/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import the class header
#import "Tile.h"

//class implementation
@implementation Tile

//property synthesis
@synthesize bounds = _bounds;
@synthesize texture = _texture;
@synthesize tileID = _tileID;
@synthesize attrs = _attrs;

//init method - initializes a Tile instance
- (id) initWithBounds: (SDLRect*) newBounds
	   andTexture: (SDLTexture*) newTexture
		andID: (TileID) newID
	      andAttrs: (TileAttribute) newAttrs {
	//call the superclass init method
	self = [super init];

	//verify that the call succeeded
	if(self) { //if the call succeeded
		//then init the fields
		_bounds = newBounds;
		[_bounds retain];
		_texture = newTexture;
		[_texture retain];
		_tileID = newID;
		_attrs = newAttrs;
	}

	//and return the instance
	return self;
}

//dealloc method - deallocates a Tile instance
- (void) dealloc {
	[_bounds release]; //release the bounds field
	[_texture release]; //release the texture field
	[super dealloc]; //and call the superclass dealloc method
}
	
//copyWithZone method - copies a Tile instance with an allocation zone
- (id) copyWithZone: (NSZone*) zone {
	//create a copy of the instance
	Tile* tileCopy = [[[self class] allocWithZone: zone] init];

	//populate its fields
	if(tileCopy) {
		tileCopy->_bounds = [[SDLRect alloc] initWithX: _bounds.x							andY: _bounds.y
						andWidth: _bounds.width
					       andHeight: _bounds.height];
		tileCopy->_texture = [_texture retain];
		tileCopy->_tileID = _tileID;
		tileCopy->_attrs = _attrs;
	}


	//and return the copy
	return tileCopy;
}

@end //end of implementation
