/*
 * TileAttribute.h
 * Enumerates attributes of terrain tiles
 * Created on 3/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

//enum definition
typedef enum {
	WALK, //walkable tile
	NOWALK, //non-walkable tile
	DMG_ENRGY, //damages energy while being walked on
	DMG_HP, //damages health while being walked on
} TileAttribute;

//end of header
