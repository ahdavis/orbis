# Makefile for Orbis
# Compiles the code for the game
# Created on 3/24/2019
# Created by Andrew Davis
#
# Copyright (C) 2019  Andrew Davis
#
# This program is free software: you can redistribute it and/or modify   
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# define the compiler
CC=gcc

# define the compiler flags
CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall

# define state-specific compiler flags
debug: CFLAGS += -g

# define linker flags
LDFLAGS=-lgnustep-base -lobjc -lobjcsdl -lm

# retrieve source code for the game
MAIN=$(shell ls src/*.m)
WRLD=$(shell ls src/world/*.m)
UTIL=$(shell ls src/util/*.m)

# list the source code for the game
SOURCES=$(MAIN) $(WRLD) $(UTIL)

# compile the source code for the game
OBJECTS=$(SOURCES:.m=.o)

# define the name of the executable package
EXECUTABLE=orbis

# start of build rules

# rule for building the game without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# rule for compiling the game without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(OBJECTS) obj/ 
	mkdir ./tmp
	mv -f $@ ./tmp/
	find . -type f -name '*.d' -delete
	cp -r ./Resources ./tmp/ 
	./makeself.sh ./tmp $@ "Orbis" ./$@
	mv -f $@ ./bin/
	rm -rf ./tmp


# rule for compiling the game with debug symbols
debug: $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(OBJECTS) obj/
	mv -f $(EXECUTABLE) bin/ 
	find . -type f -name '*.d' -delete
	cp -r ./Resources ./bin/ 

# rule for compiling source code to object code
.m.o:
	$(CC) $(CFLAGS) $< -o $@

# rule for installing the game
# REQUIRES ROOT
install:
	cp ./bin/$(EXECUTABLE) /usr/local/bin/ 

# rule for cleaning the workspace
clean:
	rm -rf bin
	rm -rf obj 

# end of Makefile
