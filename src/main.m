/*
 * main.m
 * Main code file for Orbis
 * Created on 3/24/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
#import <Foundation/Foundation.h>
#import <objcsdl/objcsdl.h>
#import <stdlib.h>
#import "./util/registerTiles.h"

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//create an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	//log the startup
	NSLog(@"Starting Orbis!");

	//create an SDL context
	SDLContext* ctxt = [[SDLContext alloc] init];

	//create a window object
	SDLWindow* win = [[SDLWindow alloc] initWithTitle: @"Orbis"
				andWidth: 640 andHeight: 480];

	//create a renderer from the window
	SDLRenderer* rend = [[SDLRenderer alloc] initWithWindow: win];

	//register the tiles
	registerTiles(rend);

	//get the path to the font file
	NSString* fontPath = [[NSBundle mainBundle] 
				pathForResource: @"blackops" 
			 	ofType: @"ttf" 
			  	inDirectory: @"fonts"];

	//create a font object from the font file
	SDLFont* font = [[SDLFont alloc] initWithPath: fontPath 
					      andSize: 36];

	//create a text element from the font and the text
	SDLTextElement* text = [[SDLTextElement alloc] 
				initWithText: @"Orbis v0.1.0"
				     andFont: font
				   withColor: [[SDLColor alloc] 
				 		initWithRed: 0x00
						   andGreen: 0x00
						    andBlue: 0x00]];

	//create a texture from the text
	SDLTexture* tex = [[SDLTexture alloc] initWithText: text
					andRenderer: rend];

	//create an event handler
	SDLEventHandler* handler = [[SDLEventHandler alloc] init];

	//declare a sentinel variable
	bool quit = NO;

	//main loop
	while(!quit) {
		//event loop
		while([handler pollNextEvent] != 0) {
			//get the current event
			SDLEvent* evnt = handler.currentEvent;

			//and process it
			if([evnt triggeredBy: EVNT_QUIT]) {
				NSLog(@"Quitting...");
				quit = YES;
			}
		}

		//fill the window with white
		[rend setDrawColor: [[SDLColor alloc] initWithRed: 0xFF
							andGreen: 0x00
							 andBlue: 0x00]];
		[rend clearScreen];

		//render the texture centered on the screen
		int x = (win.width - tex.element.surface.width) / 2;
		int y = (win.height - tex.element.surface.height) / 2;
		[rend renderTexture: tex atX: x andY: y];

		//refresh the screen
		[rend refreshScreen];

	}

	//release the texture
	[tex release];

	//release the text
	[text release];

	//release the font object
	[font release];

	//release the renderer
	[rend release];

	//release the window
	[win release];

	//release the context
	[ctxt release];

	//log the shutdown
	NSLog(@"Shutting down Orbis!");

	//drain the pool
	[pool drain];

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program
