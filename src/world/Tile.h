/*
 * Tile.h
 * Declares a class that represents a terrain tile
 * Created on 3/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
#import <Foundation/Foundation.h>
#import <objcsdl/objcsdl.h>
#import "TileID.h"
#import "TileAttribute.h"

//class declaration
@interface Tile : NSObject <NSCopying> {
	//fields
	SDLRect* _bounds; //the bounds of the Tile in the game world
	SDLTexture* _texture; //the image associated with the Tile
	TileID _tileID; //the ID of the Tile
	TileAttribute _attrs; //the attributes of the Tile
}

//property declarations
@property (retain) SDLRect* bounds;
@property (readonly) SDLTexture* texture;
@property (readonly) TileID tileID;
@property (readonly) TileAttribute attrs;

//method declarations

//initializes a Tile instance
- (id) initWithBounds: (SDLRect*) newBounds 
	andTexture: (SDLTexture*) newTexture 
	andID: (TileID) newID
	andAttrs: (TileAttribute) newAttrs;

//deallocates a Tile instance
- (void) dealloc;

//copies a Tile instance
- (id) copyWithZone: (NSZone*) zone;

@end //end of header
